Create or replace view PRODUCTION.SUPPLY.BCOM_INVENTORY_GAP as 
With
  HW as
    (
     Select 
     ph.recordsnapshotdate as "Recordsnapshotdate",
     scraped_date as "Scraped_Date",  
     ph.idproperty,
     idpropertychain,
     propertyname,
     ci.city,
     CASE WHEN co.countryiso2 = 'GB' THEN 'United Kingdom' ELSE (co.country) END AS Country, 
     CASE WHEN PT.propertytype = 'HOSTEL' THEN PT.propertytype ELSE 'NON-HOSTEL' END AS PROPERTYTYPE,     
     Market,
     ahc,
     hasfuturebedsavailable,
     count(distinct case when pf.IDFACILITY in (24,92) then ph.idproperty end) as Has_Shared_Facilities      
     from
     "PRODUCTION"."REPORTING"."LU_PROPERTYHISTORY" ph
     Inner join "PRODUCTION"."REPORTING"."LU_CITY" ci on ci.idcity = ph.idcity
     Inner join "PRODUCTION"."REPORTING"."LU_COUNTRY" co on co.idcountry = ph.idcountry
     Inner join "PRODUCTION"."REPORTING"."LU_PROPERTYTYPE" PT on PT.IDPROPERTYTYPE = ph.IDPROPERTYTYPE
     Inner join "PRODUCTION"."SUPPLY"."RH_GMTMARKET" MKT on MKT.idproperty = ph.idproperty
     Left join  "PRODUCTION"."REPORTING"."LU_PROPERTYFACILITY" pf on pf.idproperty = ph.idproperty
     Inner join "PRODUCTION"."SUPPLY"."FT_BOOKINGINVENTORY" b on b.scraped_date = ph.recordsnapshotdate 
     Where
     idpropertystatus = 1
//     and ph.idproperty IN (1569,470)
     and b.scraped_date >= '2021-03-01' and dayname(b.scraped_date) = 'Sat'
     and not (contains(upper(propertyname),'TEST') or contains(upper(propertyname),'MICROSITE') or contains(upper(propertyname),'HOSTELWORLD'))
     Group by 
     1,2,3,4,5,6,7,8,9,10,11
),

  Closed_Period as
     (
      Select 
      idproperty, to_date(date_trunc('week',LASTMODIFIED)) as CPDATE,
      count(distinct case when iddatestart <= LASTMODIFIED and iddateend >= LASTMODIFIED then idproperty end) as In_Closed_Period
      From 
       "PRODUCTION"."REPORTING"."LU_PROPERTYCLOSEDPERIOD"
      WHERE
      iddateend >= dateadd(day,-1,LASTMODIFIED)
      Group by
      idproperty,CPDATE

),
  HW_Av as
      (
      Select  
      //iddatecaptured,
      //date_trunc('week',iddatecaptured) as "AV_Week_Start",
      idproperty,
      case when count(distinct case when contains(upper(roomtype),'DORM') then av.idproperty end) >0 then 'TRUE' else 'FALSE' end as "HW_HAD_DORMS_AVAILABLE"
      From "PRODUCTION"."REPORTING"."FT_AVAILABILITY" av 
      left join "PRODUCTION"."REPORTING"."LU_ROOMTYPE" rt on rt.idroomtype = av.idroomtype
      Where
      //av.iddatecaptured ='2021-05-22' and 
      av.iddateavailable >='2019-01-01' and
   //   av.iddateavailable <= dateadd(DAY,90,av.iddatecaptured) and
      bedsunsold>0
       group by    
       idproperty
),

  Bcom as
        (
        SELECT
        b.SCRAPED_DATE,         
        to_number(b.PROPERTY_ID) as "BC_Property_ID",
        to_number(b.HW_PROPERTY_ID) as "BC_HW_PROPERTY_ID",
        b.OTA_URL,  
        b.PROPERTY_NAME,
        b.CITY,
        CASE WHEN b.country = 'GB' THEN 'United Kingdom' ELSE (co.country) END AS Country,
            Case when co.country in ('England','Ireland','Northern Ireland','Scotland','Wales','United_Kingdom') then 'UK & Ireland'
            when b.country in ('GB') then 'UK & Ireland'            
            when co.country in ('Bahamas','Belize','Curaçao','Costa Rica','Cuba','Dominican Republic','El Salvador','Guadeloupe','Guatemala','Haiti','Honduras','Jamaica','Mexico','Nicaragua','Panama','Puerto Rico','Trinidad and Tobago','Canada','US Virgin Islands','USA','Barbados','Bermuda','British Virgin Islands','Cayman Islands','Grenada','Montserrat','Saint Martin','Saint Pierre And Miquelon','Saint Vincent And The Grenadines') then 'North & Central America'
            when co.country in ('Australia','Cook Islands','French Polynesia','New Zealand','Samoa','Vanuatu','American Samoa','Fiji','Guam','Kiribati','Micronesia','New Caledonia','Niue','Northern Mariana Islands','Palau','Papua New Guinea','Solomon Islands','Tonga') then 'Oceania'
            when co.country in ('Argentina','Aruba','Bolivia','Brazil','Chile','Colombia','Ecuador','Martinique','Netherlands Antilles','Paraguay','Peru','Saint Lucia','Uruguay','Venezuela','Anguilla','Antigua And Barbuda','Dominica','French Guiana','Guyana','Saint Kitts And Nevis','Suriname','Turks And Caicos Islands') then 'South America'
            when co.country in ('Botswana','Cape Verde','Egypt','Ethiopia','Gambia','Ghana','Kenya','Madagascar','Malawi','Morocco','Mozambique','Namibia','Rwanda','Senegal','South Africa','Swaziland','Tanzania','Tunisia','Uganda','Zambia','Zimbabwe','Croatia','Denmark','Finland','Iceland','Norway','Sweden','Belgium','France','Liechtenstein','Luxembourg','Monaco','Netherlands','Switzerland','Algeria','Angola','Benin','Bouvet Island','Burkina Faso','Burundi','Cameroon','Congo','Djibouti','Faroe Islands','Greenland','Guinea','Lesotho','Mali','Mauritania','Mauritius','Nigeria','Reunion','Sao Tome And Principe','Seychelles','Sierra Leone','Togo','Albania','Armenia','Belarus','Bosnia And Herzegovina','Bulgaria','Czech Republic','Estonia','Georgia','Kosovo','Latvia','Lithuania','Macedonia','Moldova','Montenegro','Poland','Romania','Russia','Serbia','Slovakia','Slovenia','Ukraine','Austria','Germany','Hungary') then 'N.E.W. Europe & Africa'
            when co.country in ('Israel','Iran','Jordan','United Arab Emirates','Andorra','Cyprus','Gibraltar','Malta','San Marino','Turkey','Greece','Italy','Portugal','Spain','Vatican City','Palestine','Lebanon','Qatar','Saudi Arabia', 'Oman', 'Bahrain', 'Kuwait') then 'Southern Europe'
            when co.country in ('Azerbaijan','South Korea','Philippines','Kyrgyzstan','Laos','Vietnam','Kazakhstan', 'Uzbekistan','North Korea','Cambodia','Kazakhstan') then 'Asia - Squad 1'
            when co.country in ('Macau','Taiwan','India','Indonesia','China','Hong Kong','Brunei', 'Maldives', 'East Timor','Bhutan','Sri Lanka','East Timor') then 'Asia - Squad 2'
            when co.country in ('Singapore','Malaysia','Japan','Mongolia','Myanmar','Nepal','Bangladesh', 'Tajikistan', 'Pakistan') then 'Asia - Squad 3'
            when co.country in ('Thailand') then 'Asia - Squad 4'
            when city in ('Skopje','Struga','Bitola','Ohrid') then 'N.E.W. Europe & Africa'
            else 'Other' end as "BCMARKET",       
        UPPER(b.PROPERTY_TYPE) AS "PROPERTY_TYPE",
        UPPER(b.HAS_AVAILABILITY) AS "HAS_AVAILABILITY",
//        b.MIN_DORM_PRICE_EUR,
//        b.MIN_PRIVATE_PRICE_EUR,
//        b.GENIUS_PARTICIPANT,
//        b.MOBILE_RATES_PARTICIPANT,
        HadDormAvailability
        FROM 
        "PRODUCTION"."SUPPLY"."FT_BOOKINGINVENTORY" b
        LEFT JOIN
           (Select
            scraped_date,            
            property_id,
            case when count(distinct case when Min_Dorm_Price_EUR is not null then property_id end) >0 then 'TRUE' else 'FALSE' end as HadDormAvailability
            from "PRODUCTION"."SUPPLY"."FT_BOOKINGINVENTORY"
            group by 
            scraped_date,
            property_id) d
            on b.scraped_date = d.scraped_date and b.property_id = d.property_id
         LEFT JOIN
              "PRODUCTION"."REPORTING"."LU_COUNTRY" co  on co.countryiso2 = case when b.COUNTRY = 'GB'then 'United Kingdom' else b.COUNTRY end// as "CountryISO"            
         WHERE b.SCRAPED_DATE  >= '2021-03-01' and dayname(b.SCRAPED_DATE) = 'Sat'
//          and b.hw_property_id  IN (1569,470)
 )

SELECT
'Hostelworld' as "SITE", 
CASE WHEN HW.idproperty = Bcom."BC_HW_PROPERTY_ID" then TRUE ELSE FALSE END AS "MAPPED",
HW."Scraped_Date" as "SCRAPED_DATE",
HW.idproperty as "HW_ID_PROPERTY",
CASE WHEN "MAPPED" = TRUE THEN Bcom."BC_Property_ID" ELSE NULL END as "B.COM_ID_PROPERTY",
HW.propertyname as "PROPERTY_NAME",
HW.city as "CITY",
HW.country as "COUNTRY",
HW."PROPERTYTYPE" as "PROPERTY_TYPE",
HW.Market as "MARKET", 
//HW.ahc,
HW.hasfuturebedsavailable  as "HAS_AVAILABILITY",
//HW_AV."HW_HAD_DORMS_AVAILABLE",
CASE 
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = TRUE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'TRUE' THEN 'Mapped & Available on Both - as Hostels'
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = FALSE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'FALSE' THEN 'Mapped & Expired on Both - as Hostels'
  WHEN Bcom."BC_Property_ID" IS NULL AND HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = FALSE THEN 'HW Expired Hostels & Not Mapped'
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = FALSE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'TRUE'  THEN 'HW Expired & Available on BC (BC Hostel definition)' 
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" <> 'HOSTEL' AND HW.hasfuturebedsavailable = TRUE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'TRUE'  THEN 'BC Available Hostels Mapped to HW Available Non-Hostels'
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" <> 'HOSTEL' AND HW.hasfuturebedsavailable = FALSE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'TRUE'  THEN 'BC Available Hostels Mapped to HW Expired Non-Hostels'  
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" <> 'HOSTEL' AND HW.hasfuturebedsavailable = FALSE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'FALSE'  THEN 'BC Expired Hostels Mapped to HW Expired Non-Hostels'     
  WHEN HW.idproperty IS NULL AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'TRUE'  THEN 'BC Exclusive Hostels with Availability (BC Hostel definition)'
  WHEN HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = TRUE AND Bcom."BC_HW_PROPERTY_ID" IS NULL THEN 'HW Exclusive Hostels with Availability'
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = TRUE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'FALSE' THEN 'BC Expired Hostels & Available on HW'
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = TRUE AND Bcom."PROPERTY_TYPE" <> 'HOSTEL'  THEN 'HW Hostels Mapped to BC Available Non-Hostels'  
  WHEN HW."PROPERTYTYPE" <> 'HOSTEL' AND Bcom."BC_HW_PROPERTY_ID" IS NULL THEN 'HW Non-Hostels & Not Mapped'  
  WHEN HW.idproperty IS NULL AND Bcom."HAS_AVAILABILITY" = 'FALSE' THEN 'BC Exclusive & Expired Properties'
  WHEN HW.idproperty = Bcom."BC_HW_PROPERTY_ID" AND HW."PROPERTYTYPE" <> 'HOSTEL' AND HW.hasfuturebedsavailable = TRUE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'FALSE' then 'BC Expired Hostels Mapped to HW Non-Hostels'
  END AS "STATUS" ,
  NULL as "URL"


FROM
HW 
left join
Closed_Period on Closed_Period.idproperty = HW.idproperty and  Closed_Period.CPDATE = HW."Scraped_Date"
left join
HW_Av on HW_Av.idproperty = HW.idproperty //and HW."HW_Week_Start" = HW_Av."AV_Week_Start"
full join
Bcom on Bcom."BC_HW_PROPERTY_ID" = HW.idproperty and Bcom.scraped_date = HW."Scraped_Date"

Group by 1,2,3,4,5,6,7,8,9,10,11,12,13

UNION ALL


SELECT
 'Booking.com' as "SITE", 
  CASE WHEN HW.idproperty = Bcom."BC_HW_PROPERTY_ID" then TRUE ELSE FALSE END AS "MAPPED", 
  Bcom.Scraped_Date as "SCRAPED_DATE",
  Bcom."BC_HW_PROPERTY_ID" as "HW_ID_PROPERTY",
  Bcom."BC_Property_ID" as "B.COM_ID_PROPERTY",  
  Bcom.PROPERTY_NAME as "PROPERTY_NAME",
  Bcom.CITY as "CITY",
  Bcom.COUNTRY as "COUNTRY",
  Bcom."PROPERTY_TYPE" as "PROPERTY_TYPE",
  CASE WHEN HW."MARKET" = 'Top Chains' THEN HW."MARKET" ELSE Bcom."BCMARKET" END as "MARKET",
  Bcom."HAS_AVAILABILITY" as "HAS_AVAILABILITY",
CASE 
  WHEN "B.COM_ID_PROPERTY" IS NULL THEN NULL
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = TRUE  and Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'TRUE' THEN 'Mapped & Available on Both - as Hostels'
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = FALSE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'FALSE' THEN 'Mapped & Expired on Both - as Hostels'
  WHEN Bcom."BC_Property_ID" IS NULL AND HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = FALSE THEN 'HW Expired Hostels & Not Mapped'
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = FALSE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'TRUE'  THEN 'HW Expired & Available on BC (BC Hostel definition)' 
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" <> 'HOSTEL' AND HW.hasfuturebedsavailable = TRUE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'TRUE'  THEN 'BC Available Hostels Mapped to HW Available Non-Hostels'
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" <> 'HOSTEL' AND HW.hasfuturebedsavailable = FALSE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'TRUE'  THEN 'BC Available Hostels Mapped to HW Expired Non-Hostels'  
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" <> 'HOSTEL' AND HW.hasfuturebedsavailable = FALSE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'FALSE'  THEN 'BC Expired Hostels Mapped to HW Expired Non-Hostels'     
  WHEN HW.idproperty IS NULL AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'TRUE'  THEN 'BC Exclusive Hostels with Availability (BC Hostel definition)'
  WHEN HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = TRUE AND Bcom."BC_HW_PROPERTY_ID" IS NULL THEN 'HW Exclusive Hostels with Availability'
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = TRUE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'FALSE' THEN 'BC Expired Hostels & Available on HW'
  WHEN "MAPPED" = TRUE AND HW."PROPERTYTYPE" = 'HOSTEL' AND HW.hasfuturebedsavailable = TRUE AND Bcom."PROPERTY_TYPE" <> 'HOSTEL'  THEN 'HW Hostels Mapped to BC Available Non-Hostels'  
//  WHEN HW."PROPERTYTYPE" <> 'HOSTEL' AND Bcom."BC_HW_PROPERTY_ID" IS NULL THEN 'NON-HOSTEL'  
  WHEN HW.idproperty IS NULL AND Bcom."HAS_AVAILABILITY" = 'FALSE' THEN 'BC Exclusive & Expired Properties'
  WHEN HW.idproperty = Bcom."BC_HW_PROPERTY_ID" AND HW."PROPERTYTYPE" <> 'HOSTEL' AND HW.hasfuturebedsavailable = TRUE AND Bcom."PROPERTY_TYPE" = 'HOSTEL' AND Bcom."HAS_AVAILABILITY" = 'FALSE' then 'BC Expired Hostels Mapped to HW Non-Hostels'
  END AS "STATUS" ,
  Bcom.OTA_URL as "URL"
 
FROM
HW 
left join
Closed_Period on Closed_Period.idproperty = HW.idproperty and  Closed_Period.CPDATE = HW."Scraped_Date"
left join
HW_Av on HW_Av.idproperty = HW.idproperty //and HW."HW_Week_Start" = HW_Av."AV_Week_Start"
full join
Bcom on Bcom."BC_HW_PROPERTY_ID" = HW.idproperty and Bcom.scraped_date = HW."Scraped_Date"

Group by 1,2,3,4,5,6,7,8,9,10,11,12,13;